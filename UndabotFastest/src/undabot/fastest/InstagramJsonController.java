package undabot.fastest;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class InstagramJsonController {
private ArrayList<Model> lista;
private String url;
private JSONArray data = null;
private Model model;


public InstagramJsonController(ArrayList<Model> list,String url)
{
	this.lista= list;
	this.url=url;
	
}

public ArrayList<Model> getLista() {
	execJsonparse();
	return lista;
}
public void setLista(ArrayList<Model> lista) {
	this.lista = lista;
}

public void  execJsonparse()
	{
		
		ResponseServiceHandler sh = new ResponseServiceHandler();
		String jsonStr = sh.makeServiceCall(url, ResponseServiceHandler.GET);
		
		if (jsonStr != null) {
			try {
				JSONObject jsonObj = new JSONObject(jsonStr);
				data = jsonObj.getJSONArray("data");

				for (int i = 0; i < data.length(); i++) {

					JSONObject c = data.getJSONObject(i);
					String imageUrl = c.getJSONObject("images").getJSONObject("thumbnail").getString("url");
					String caption = c.getJSONObject("caption").getString("text");
					String user = c.getJSONObject("user").getString("username");
					model = new Model();
					model.setAutor(user);
					model.setSlika(imageUrl);
					model.setOpis(caption);
					lista.add(model);
					
					

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Log.e("ResponseServiceHandler", "Couldn't get any data from the url");
		}
		
		
		
		
	}
	
}
