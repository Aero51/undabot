package undabot.fastest;

/**
 * Created by Nikola on 19.03.14..
 */
public class Model {
    private String opis;
    private String slika;
    private String autor;

    public String getSlika() {
        return slika;
    }

    public String getAutor() {
        return autor;
    }

    public String getOpis() {
        return opis;
    }



    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }




}
