package undabot.picasso;

/**
 * Created by Nikola on 19.03.14..
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import com.squareup.picasso.Picasso;

final class AdapterList extends BaseAdapter {
    private final Context context;
    private  List<Model> lista= new ArrayList<Model>();

    public AdapterList(Context context, ArrayList<Model> list) {
        this.context = context;
        lista= list;
    }

    @Override public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder();
            holder.slika = (ImageView) view.findViewById(R.id.slika);
            holder.autor = (TextView) view.findViewById(R.id.autor);
            holder.opis = (TextView) view.findViewById(R.id.opis);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

    holder.autor.setText(lista.get(position).getAutor());
    holder.opis.setText(lista.get(position).getOpis());

        Picasso.with(context)
                .load(lista.get(position).getSlika())
                .placeholder(R.drawable.placeholder)
               // .error(R.drawable.no_image)
                .resizeDimen(R.dimen.list_detail_image_size, R.dimen.list_detail_image_size)
                .centerInside()
                .into(holder.slika);

        return view;
    }

    @Override public int getCount() {
        return lista.size();
    }

    @Override public String getItem(int position) {
        return null;
    }

    @Override public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        ImageView slika;
        TextView autor;
        TextView opis;
    }
}