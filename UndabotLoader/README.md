Example Android App for loading instagram popular photos
==================================================

Implemented fetching of json data with Loader, async photo download.
Used Action bar.
##References: 
[Life Before Loaders](www.androiddesignpatterns.com/2012/07/loaders-and-loadermanager-background.html)

[How to Use Loaders in Android](www.grokkingandroid.com/using-loaders-in-android/)

[The Android Loaders Story](learning.icewheel.org/2013/04/the-android-loaders-story.html)

##Todo:
 -implement reusing of frags in main activity

-prevent redownloading of pics on config change

-implement refresh button in action bar(upper right corner) which downloads new set of popular photos