package undabot.fragments;

import java.util.ArrayList;
import java.util.List;

import undabot.actiontabs.AdapterList;
import undabot.model.Model;
import undabot.rest.InstagramJsonController;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class InstaListFragment extends ListFragment implements
		LoaderCallbacks<ArrayList<Model>> {

	// private ProgressDialog pDialog;
	private static String url = "https://api.instagram.com/v1/media/popular?client_id=e5d3b5e00f70488fa244e02e9ec49e86";
	private ArrayList<Model> lista;
	private AdapterList adapter;
	private LayoutInflater mInflater;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		// setRetainInstance(true);

		mInflater = LayoutInflater.from(getActivity());

		if (adapter == null) {
			lista = new ArrayList<Model>();
			// instaJsonController= new InstagramJsonController(lista, url);
			adapter = new AdapterList(getActivity(), lista);

		}
		getListView().setAdapter(adapter);

		LoaderManager lm = getLoaderManager();

		lm.initLoader(0, null, this);

	}

	@Override
	public Loader<ArrayList<Model>> onCreateLoader(int id, Bundle args) {

		return new DataListLoader(getActivity());
	}

	@Override
	public void onLoadFinished(Loader<ArrayList<Model>> arg0,
			ArrayList<Model> arg1) {
		lista = arg1;
		adapter = new AdapterList(getActivity(), lista);
		getListView().setAdapter(adapter);
		// adapter.notifyDataSetChanged();
		setListShown(true);
		Log.d("App", "onLoadFinished(): done loading!" + lista.size());

	}

	@Override
	public void onLoaderReset(Loader<ArrayList<Model>> arg0) {

		// TODO Auto-generated method stub

	}

	public static class DataListLoader extends
			AsyncTaskLoader<ArrayList<Model>> {

		ArrayList<Model> mlista;
		InstagramJsonController instaJsonController;

		public DataListLoader(Context context) {

			super(context);
		}

		@Override
		public ArrayList<Model> loadInBackground() {
			System.out.println("DataListLoader.loadInBackground");

			ArrayList<Model> dlista = new ArrayList<Model>();
			instaJsonController = new InstagramJsonController(
					dlista,
					"https://api.instagram.com/v1/media/popular?client_id=e5d3b5e00f70488fa244e02e9ec49e86");
			dlista = instaJsonController.getLista();
			Log.d("App", "Lista gotova: done loading!" + dlista.size());

			return dlista;
		}

		@Override
		public void deliverResult(ArrayList<Model> data) {

			if (isReset()) {
				if (data != null) {

					Log.d("App", "Lista prazna: deliverresult");
				}

			}
			// mlista = data;
			ArrayList<Model> oldList = data;
			mlista = oldList;

			if (isStarted()) {
				// If the Loader is currently started, we can immediately
				// deliver its results.
				Log.i("App", "+++ Delivering results to the LoaderManager for"
						+ " the ListFragment to display! +++");
				super.deliverResult(data);
			}

			if (oldList != null) {
				Log.i("App",
						"+++ ReleasingReleasing any old data associated with this Loader. +++");
			}

		}

		@Override
		protected void onStartLoading() {
			if (mlista != null) {
				Log.i("App",
						"+++ Delivering previously loaded data to the client...");

				deliverResult(mlista);
			}
			if (takeContentChanged() || mlista == null) {
				forceLoad();
			}

		}

		/**
		 * Handles a request to stop the Loader.
		 */
		@Override
		protected void onStopLoading() {
			// Attempt to cancel the current load task if possible.
			Log.i("App", "+++ onStopLoading() called! +++");
			cancelLoad();
		}

		/**
		 * Handles a request to completely reset the Loader.
		 */
		@Override
		protected void onReset() {
			Log.i("App", "+++ onReset() called! +++");

			// Ensure the loader is stopped
			onStopLoading();

			// At this point we can release the resources associated with 'apps'
			// if needed.
			if (mlista != null) {

				mlista = null;
			}
		}

	}

}
