package com.example.undabotcustominsta;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.graphics.drawable.Drawable;

public class DrawableDownloader {
	
	
	
	public  Drawable downloadDrawable(String url) {  
	    try {  
	        InputStream is = getInputStream(url);
	        Drawable drawable = Drawable.createFromStream(is, url);
	       // putDrawableInCache(url,drawable);  
	        return drawable;  

	    } catch (MalformedURLException e) {  
	        e.printStackTrace();  
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }  

	    return null;  
	}  
	
	private  InputStream getInputStream(String urlString) throws MalformedURLException, IOException {
	    URL url = new URL(urlString);
	    URLConnection connection;
	    connection = url.openConnection();
	    connection.setUseCaches(true); 
	    connection.connect();
	    InputStream response = connection.getInputStream();

	    return response;
	}

}
