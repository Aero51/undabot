package com.example.undabotcustominsta;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by Nikola on 19.03.14..
 */
public class Model {
    private String opis;
    private String urlSlike;
    private String autor;
    private Bitmap slika;
    private Drawable drawable;
    private AdapterList adapter;
    
    
  
    
    public AdapterList getAdapter() {
		return adapter;
	}

	public void setAdapter(AdapterList adapter) {
		this.adapter = adapter;
	}

	

    public String getUrlSlike() {
        return urlSlike;
    }

    public String getAutor() {
        return autor;
    }

    public String getOpis() {
        return opis;
    }



    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setSlika(String slika) {
        this.urlSlike = slika;
    }

	public Bitmap getSlika() {
		return slika;
	}

	public void setSlika(Bitmap slika) {
		this.slika = slika;
	}
	public Drawable getDrawable() {
		return drawable;
	}

	public void setDrawable(Drawable drawable) {
		this.drawable = drawable;
	}
	
	
	
	public void loadImageDrawable(AdapterList sta) {
        // HOLD A REFERENCE TO THE ADAPTER
        this.adapter = sta;
       
        if (urlSlike != null && !urlSlike.equals("")) {
            new ImageLoadTaskDrawable().execute(urlSlike);
        }
    }
	
	
private class ImageLoadTaskDrawable extends AsyncTask<String, String, Drawable>
{



	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		 Log.i("UndabotCustomInsta", "Loading image...");
		//super.onPreExecute();
	}

	@Override
	protected Drawable doInBackground(String... param) {
		// TODO Auto-generated method stub
		 Log.i("UndabotCustomInsta", "Attempting to load image URL: " + param[0]);
		 try {
			 DrawableDownloader downloader = new DrawableDownloader();
			Drawable d= downloader.downloadDrawable(param[0]);
			return d;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	@Override
	protected void onProgressUpdate(String... progress) {
		// TODO Auto-generated method stub
		//super.onProgressUpdate(values);
		
	}

	
	@Override
	protected void onPostExecute(Drawable result) {
		// TODO Auto-generated method stub
		if(result != null)
		{
			Log.i("UndabotCustomInsta", "Successfully loaded " + autor + " image"
					+"Url: " + urlSlike);
			drawable= result;
			if(adapter != null)
			{
				adapter.notifyDataSetChanged();
				
				
			}
		}else {Log.e("ImageLoadTaskBitmap", "Failed to load " + autor + " image"
				+"Url: " + urlSlike);
		}
			
		}
		
	}

}



