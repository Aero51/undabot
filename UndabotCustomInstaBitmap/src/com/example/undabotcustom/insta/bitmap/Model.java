package com.example.undabotcustom.insta.bitmap;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by Nikola on 19.03.14..
 */
public class Model {
    private String opis;
    private String urlSlike;
    private String autor;
    private Bitmap slika;
    private AdapterList adapter;
    
    
   /* public Model (String urlSlike)
    {
    	this.urlSlike= urlSlike;
    	
    }*/
    
    public AdapterList getAdapter() {
		return adapter;
	}

	public void setAdapter(AdapterList adapter) {
		this.adapter = adapter;
	}

	

    public String getUrlSlike() {
        return urlSlike;
    }

    public String getAutor() {
        return autor;
    }

    public String getOpis() {
        return opis;
    }



    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setSlika(String slika) {
        this.urlSlike = slika;
    }

	public Bitmap getSlika() {
		return slika;
	}

	public void setSlika(Bitmap slika) {
		this.slika = slika;
	}
	
	
	
	
	
	public void loadImageBitmap(AdapterList sta) {
        // HOLD A REFERENCE TO THE ADAPTER
        this.adapter = sta;
       
        if (urlSlike != null && !urlSlike.equals("")) {
            new ImageLoadTaskBitmap().execute(urlSlike);
        }
    }


private class ImageLoadTaskBitmap extends AsyncTask<String, String, Bitmap>
{



	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		 Log.i("UndabotCustomInsta", "Loading image...");
		//super.onPreExecute();
	}

	
	@Override
	protected Bitmap doInBackground(String... param) {
		// TODO Auto-generated method stub
		 Log.i("UndabotCustomInsta", "Attempting to load image URL: " + param[0]);
		 try {
			Bitmap b= BitmapDownloader.getBitmapFromURL(param[0]);
			return b;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	@Override
	protected void onProgressUpdate(String... progress) {
		// TODO Auto-generated method stub
		//super.onProgressUpdate(values);
	}

	
	@Override
	protected void onPostExecute(Bitmap result) {
		// TODO Auto-generated method stub
		if(result != null)
		{
			Log.i("UndabotCustomInsta", "Successfully loaded " + autor + " image"
					+"Url: " + urlSlike);
			slika= result;
			if(adapter != null)
			{
				adapter.notifyDataSetChanged();
				
				
			}
		}else {Log.e("ImageLoadTaskBitmap", "Failed to load " + autor + " image"
				+"Url: " + urlSlike);
		}
			
		}
		
	}














}



