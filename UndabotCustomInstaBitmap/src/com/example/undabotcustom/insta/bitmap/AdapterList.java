package com.example.undabotcustom.insta.bitmap;

import java.util.ArrayList;

import com.example.undabotcustominsta.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterList extends BaseAdapter {
	 private final Context context;
	   
	 
	    private ArrayList<Model> items = new ArrayList<Model>();
	 
	    public AdapterList(Context context, ArrayList<Model> list) {
	       // mInflater = LayoutInflater.from(context);
	    	this.context= context;
	        this.items = list;
	    }
	 
	    public int getCount() {
	        return items.size();
	    }
	 
	    public Model getItem(int position) {
	        return items.get(position);
	    }
	 
	    public long getItemId(int position) {
	        return position;
	    }
	 
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ViewHolder holder;
	        Model model = items.get(position);
	        if (convertView == null) {
	           // convertView = mInflater.inflate(R.layout.list_item, null);
	        	convertView = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
	            holder = new ViewHolder();
	            
	            holder.slika = (ImageView) convertView.findViewById(R.id.slika);
	            holder.autor = (TextView) convertView.findViewById(R.id.autor);
	            holder.opis = (TextView) convertView.findViewById(R.id.opis);
	            convertView.setTag(holder);
	        } else {
	            holder = (ViewHolder) convertView.getTag();
	        }
	        
	        holder.autor.setText(model.getAutor());
	        holder.opis.setText(model.getOpis());
	        
	        if (model.getSlika() != null) {
	            holder.slika.setImageBitmap(model.getSlika());
	           
	        } else {
	               
	            holder.slika.setImageResource(R.drawable.ic_launcher);
	        }
	        return convertView;
	    }
	 
	    static class ViewHolder {
	    	 ImageView slika;
	         TextView autor;
	         TextView opis;
	    }
	 
	}
